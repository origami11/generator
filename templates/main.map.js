{
    "entry": "index.php",
    "installPage": "install.php",
    "bootPage": "classes/boot.php",

    "search": "classes/controller/search.php",
    "searchpage": "classes/view/search_page.php",
    "searchresults": "classes/view/result_page.php",

    "scm_tables": "install/schema.php",
    "tables": "install/install.sql",
    "index": "classes/view/index.php",

    "classModel": "classes/query/{className:lowercase}.php",
    "renderListPage": "classes/view/{className:lowercase}_list.php",
    "renderEditPage": "classes/view/{className:lowercase}_form.php",
    "controller": "classes/controller/{className:lowercase}.php"
}