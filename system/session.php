<?php

class Date {
    private $time;
    public function getYear() {
        $date = getdate($time);
        return $date['year'];
    }
}

/* ���짮��⥫� */
class User {   
    function findById($id) {
        $stmt = $this->db->prepare("SELECT * FROM user WHERE id_user = :id");
        $stmt->execute(array('id' => $id));
        return $stmt->fetch();
    }

    function find($login, $password) {
        $stmt = $this->db->prepare("SELECT * FROM user WHERE login = :login AND password = :password");
        $stmt->execute(array('login' => $login, 'password' => $password));
        return $stmt->fetch();
    }
}

/* ��ࠢ����� ��ᨥ� */
class Session {
    public $user = null;
    static $instance = null;

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new Session();
        }
        return self::$instance;
    }

    public function __construct($person) {
        if (isset($_SESSION['user'])) {
            $user = $person->findById($_SESSION['user']);
            if ($user) {
                $this->user = $user;
            }
        }
    }

    public function enterUser() {
        if ($user) {
            $_SESSION['user'] = $user['id_user'];
            $session->user = $user;
        }
    }

    public function exitUser() {
       unset($_SESSION['user']);
    }
}

/* ��ࠢ����� ���ਧ�樥� 
  + � 蠡���� ��� ��� �������� 䫠� �� �室
*/
class Controller_Session {    

    public function action_login() {
        $login = Arr::get($_POST, 'login');
        $password = Arr::get($_POST, 'password');
        if ($login && $password) {
            $session = Session::getInstance();
            $session->enterUser($login, md5($password));
        }
    }

    public function action_logout() {
        $session = Session::getInstance();
        $session->exitUser();
    }
}
