<?php

require_once dirname(__FILE__) . '/session.php';
require_once dirname(__FILE__) . '/json.php';
require_once dirname(__FILE__) . '/system.php';

$db = new PDO(PDO_DATABASE, PDO_LOGIN, PDO_PASSWORD);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if (substr(PDO_DATABASE, 0, 6) == 'sqlite') {
    function sqliteLower($str) {
        return mb_strtolower($str, 'UTF-8');
    }

    $db->sqliteCreateFunction('LOWER', 'sqliteLower', 1);
}

session_start();

$person = new User($db);
$session = new Session($person);

$router = new Router(array('class' => MAIN_CLASS, 'method' => 'index'));
$router->dispatch($db);
