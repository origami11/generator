<?php

// Файл обновления или установки 

function message($class, $message) {
    echo "<div class='$class'>$message</div>";
}

function exec_sql($db, $file) {
    $sql = file_get_contents($file);
    foreach(explode(";", $sql) as $query) {
        $db->query("$query");
    }
}

function make_dir($name) {
    if (!file_exists($name)) {
        mkdir($name);
        message('ok', "Directory <b>'$name'</b> created");
        return;
    }
    message('ok', "The directory <b>'$name'</b> already exists");
}

function install_database() {
    $file = 'database/data.dat';
    if (!file_exists('install/schema.php')) {
        message('error', "There is no need to update the database");
        return;
    }

    if (file_exists($file)) {
        $file_new = 'database/data.dat';
        $file_old = 'database/data.old.dat';
        if (file_exists($file_old)) unlink($file_old);
        rename($file_new, $file_old);
    
        $db_old = new PDO("sqlite:$file_old");
        $db_old->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $db = new PDO("sqlite:$file_new");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        exec_sql($db, 'install/install.sql');

        include "install/current.php";

        try {
            foreach($schema as $table => $item) {
                $keys = implode(",", $item);
                $stmt = $db_old->prepare("SELECT $keys FROM $table");
                $stmt->execute();
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($data as $row) {
                    $values = implode(",", array_map(array($db, 'quote'), $row));
                    $query = "INSERT INTO $table ($keys) VALUES ($values)";
                    $stmt = $db->query($query);
                }      
            }
    
            unlink('install/current.php');
            rename('install/schema.php', 'install/current.php');
        }  catch(Exception $e) {
            message('error', "Error creating database");
            return false;
        }
    } else {
        $db = new PDO("sqlite:$file");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        rename('install/schema.php', 'install/current.php');
        exec_sql($db, 'install/install.sql');
    }
    message('ok', "Database created");
    return true;
}

?>

<!doctype html>
<html>
<head>
    <title>Install</title>
    <style type="text/css">
    body {font-family: Tahoma;}
    button {border: 1px solid #cfcfcf; background: #efefef; cursor: pointer;}
    button:hover {background: #ccf;}
    .ok {background: #b3ee9b; margin: 3px 0px; padding: 4px 4px; font-size: 12px;}
    .error {background: #ee9b9d; margin: 3px 0px; padding: 4px 4px; font-size: 12px;}
    </style>
</head>
<body>
    <?php if (!isset($_GET['install'])): ?>
    <form method="post" action="?install=ok">
        <button type="submit">install</button>
    </form>
    <?php else: ?>
    <form method="post" action="index.php">
        <button type="submit">index</button>
    </form>
    <?php 
        make_dir('database');
        make_dir('session');
        make_dir('uploads');
        install_database();
    ?>
    <?php endif ?>
</body>
</html>
