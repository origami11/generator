<?php

function postfix($s, $array) {
    $result = "";
    foreach($array as $item) {
        $result .= $item . "/";
    }
    return $result;
}

function concat() {
    $args = func_get_args();
    return implode($args);
}

function script($path) {
    return "<script type=\"text/javascript\" src=\"" . ASSET_PATH . $path . "\"></script>\n";
}

function style($path, $rel = 'stylesheet') {
    return "<link rel=\"$rel\" type=\"text/css\" href=\"" . ASSET_PATH . $path . "\"/>\n";
}

function getPages($page, $count, $onpage) {
    $pages = ceil($count / $onpage);
    $result = array();
    $range = 5;        
    // ������ ������������� ������ ������ ��������� � �������
    for ($i = max($page - $range, 1); $i <= min($pages, $page + $range); $i++) {
        $result[] = array($i, $i == $page);
    }
    return array(
        'start' => ($page - 1) * $onpage + 1, 
        'end' => min($count, $page * $onpage),
        'numbers' => $result, 
        'previous' => max($page - 1, 1), 
        'last' => $pages, 
        'next' => min($pages, $page + 1));
}

function toFormDate($s) {
    if ($s) {
        return date('Y-m-d', $s);
    }
    return '';
}

function toFormTime($s) {
    if ($s) {
        return date('H:i', $s);
    }
    return '';
}

function toFormDateTime($s) {
    if ($s) {
        return date('Y-m-d', $s) . 'T' . date('H:i', $s) . 'Z';
    }
    return '';
}

function formatDate($format, $time) {
    return $time ? date($format, $time) : '';
}

function isEmail($s) {
    return filter_var($s, FILTER_VALIDATE_EMAIL);
}

function isDate($s) {
    return true;
}

function isTime($s) {
    return true;
}

function isDateTime($s) {
    return true;
}

function toDate($s) {
    $m = array();
    if (preg_match('/(\d{4})-(\d{1,2})-(\d{1,2})/', $s, $m)) {
        return mktime(0, 0, 0, $m[2], $m[3], $m[1]);
    }
    return 0;
}

function toDateTime($s) {
    $m = array();
    if (preg_match('/(\d{4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2})Z/', $s, $m)) {
        return mktime($m[4], $m[5], 0, $m[2], $m[3], $m[1]);
    }
    return 0;
}

function toTime($s) {
    $m = array();
    if (preg_match('/(\d{1,2}):(\d{1,2})/', $s, $m)) {
        return mktime($m[1], $m[2]);
    }
    return 0;
}

function toBoolean($s) {
    return ((bool)$s) ? 'TRUE' : 'FALSE';
}

class Query {
    public $condition = "";
    public $order = "";
    public $limit = "";
    public $dir = "";
    public $db = null;
    private $ctrl = null;

    public function setLimit($limit, $offset) {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function addCondition($condition) {
        $this->condition .= $condition;
    }

    public function setOrder($order, $dir) {
        $this->order = $order;
        $this->dir = $dir;
    }

    public function __construct($ctrl) {
        $this->db = $ctrl->db;
        $this->ctrl = $ctrl;
    }

    public function getContext($key, $default = null) {
        return $this->ctrl->getContext($key, $default);
    }
}

class Arr {
    static public function get(array $array, $name, $default = null) {
        if (isset($array[$name])) {
            if (get_magic_quotes_gpc() && is_string($array[$name])) {
                return stripslashes($array[$name]);
            } else {
                return $array[$name];
            }
        }
        return $default;
    }

    static public function getKeys(array $array, $key) {
        $result = array();
        foreach ($array as $item) {
            $result [] = $item[$key];
        }
        return $result;
    }
}

class View {
    private $data = array();        
    public function __construct($file) {        
        $this->file = $file;
    }

    public function set($name, $value) {
        $this->data[$name] = $value;
    }

    public function inBasket($class, $item) {
        return isset($_SESSION['basket_' . $class][$item]);
    }

    public function render() {
        ob_start();
        $data = $this->data;
        include ("classes/view/" . $this->file . ".php");
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }
}

class Context {
    public $cursor = "";

    public function __construct() {
        $this->set($this->cursor);
    }

    // ��������� �������� ��� ���� ������������ ������
    public function add($key, $value, $default = null) {
        // ���� �������� ����� ������� � ������
        if ($value === null) {
            // ���� � ����� ��� �� ������� �������� �� ��������� 
            if (!isset($_SESSION[$this->cursor][$key])) {
                // ���� � ��� ��� �� ��� ����������
                if ($default === null) {
                    throw new Exception('NoValue');
                } 
                $_SESSION[$this->cursor][$key] = $default;
            }
        } else {
            // ������������� ���� ����        
            $_SESSION[$this->cursor][$key] = $value;
        }
    }

    public function set($cursor) {
        $this->cursor = $cursor;
        if (!isset($_SESSION[$this->cursor])) $_SESSION[$this->cursor] = array();
    }

    public function clear() {
        if (isset($_SESSION[$this->cursor])) $_SESSION[$this->cursor] = array();
    }

    public function get($key, $default = null) {
        if (!isset($_SESSION[$this->cursor][$key]) || $_SESSION[$this->cursor][$key] == null) {
            return $default;
        }
        return $_SESSION[$this->cursor][$key];
    }
}

// ���� ��������� �������� 
class Controller {
    private $db;
    public $path = array();

    public function __construct() {
        $this->context = new Context();
    }

    public function before() {
    }

    public function redirect($a, $b, $rest = array()) {
        $url = http_build_query(array_merge(array('method' => $b), $rest));
        header("Location: ?class=$a&$url");
        exit();        
    }

    public function after($r) {
        return $r;
    }

}

/* ������� ��� ������ � �������� */
class Basket {
    private $class = "";
    public function __construct($class) {
        $this->class = $class;
    }
    public function getKeys() {
        return array_keys(Arr::get($_SESSION, 'basket_' . $this->class, array()));
    }

    public function isEmpty() {
        return isset($_SESSION['basket_' . $this->class]) && count($_SESSION['basket_' . $this->class]) > 0;
    }

    public function clear() {
        $_SESSION['basket_' . $this->class] = array();
    }
}

class Router {
    public $list;
    public function __construct($list) {
        $this->list = $list;
    }

    public function dispatch($db) {
        $classPath = Arr::get($_GET, 'class', $this->list['class']);    
        $path = explode("/", $classPath);
        $class = array_pop($path);
        

        require_once ("classes/controller/" . $class . ".php");
        $className = "Controller_" . $class;
        $instance = new $className();
        $instance->db = $db;        
        $instance->path = $path;

        $method = Arr::get($_GET, 'method', $this->list['method']);
        $action = 'action_' . $method;
        $instance->before();
        if (method_exists($instance, $action)) {
            echo $instance->after(call_user_func(array($instance, $action)));
        } else if ($instance->extension && method_exists($instance->extension, $action)) {
            $instance->extension->parent = $instance;
            echo $instance->after(call_user_func(array($instance->extension, $action)));
        } else {
            echo $instance->after(call_user_func(array($instance, 'action_index')));
        }
    }
}
