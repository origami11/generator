
var meta = require("./meta-ast.js")
    , tast = require("./ast.js")
    , path = require("path")
    , fs = require("fs")
    , P = require("./jsparse.js")
    , PP = require("./print.js")
    , Grammar = require("./grammar.js").grammar
    , MetaGrammar = require("./meta-grammar.js").grammar;

var encoding = "utf-8";
var print = console.log;

var dirList = ["./"];
var templateExtension = "stp";

function rawEncoding(text) { 
    return text; 
}

function printObject(o) {
    for(var i in o) {
        print(i, ":", o[i]);
    }
}

function setConverter(name) {
    encodingConverter = name;
}

function findFile(prefix, templateExtension) {
    for(var i = 0; i < dirList.length; i++) {
        var fname = dirList[i] + prefix + "." + templateExtension;
        if (path.existsSync(fname)) {
            return fname;
        }        
    }
    return null;
}

function Template(name, context) {
    this.fileName = name;
//    this.data = readFile(name);
    this.data = fs.readFileSync(name, encoding);
    this.context = context;    
    this.parseAll();
}

Template.prototype.parseAll = function() {
    var ast = Grammar.parser(new P.ParseState(this.data, 0));
    // index
    if (!ast.result) {
        throw "\n" + ast.error.getMessage(this.data, this.fileName);
    }
    ast = ast.ast;

    this.context.addMethods(ast);

    for(var i = 0; i < ast.length; i++) {
        if (ast[i] instanceof tast.ASTUsing) {
            loadTemplate(this.context, ast[i].name);
        }
    }
}

function render(context, entry, data) {
    // substitute;
    var result = "";
    if (context.methods.hasOwnProperty(entry)) {
        result = context.methods[entry].evaluate(data, context);
    }

    return result;
}

function renderToString(context, entry, def) {
    var code = render(context, entry, def);
    var text = PP.pretty_print(code);
    return text;
}

function mkPath(/**Array*/ path0) {
	if (path0.constructor != Array) path0 = path0.split(/[\\\/]/);
	var make = "";
	for (var i = 0, l = path0.length; i < l; i++) {
		make += path0[i] + '/';
		if (! path.existsSync(make)) {
			fs.mkdirSync(make);
		}
	}
}

function renderToFile (context, entry, def, file, convert) {
    convert = convert || rawEncoding;
    var code = render(context, entry, def);
    var text = PP.pretty_print(code);
//    var path = file;
//    var dir = FilePath.dir(path);
    var dir = path.dirname(file);
    mkPath(dir);
//    IO.writeFile(dir, FilePath.fileName(path), text);
///    IO.saveFile(dir, FilePath.fileName(file), text);
    fs.writeFileSync(file, convert(text));
}

function loadModelAst(base, modelFile, ext) {
    result = meta.Environment;
    print(modelFile);

//    var data = readFile(modelFile);
    var data = fs.readFileSync(modelFile, encoding);
    var ast = MetaGrammar.parser(new P.ParseState(data, 0));
 
    if (!ast.result) {    
        throw "\n" + ast.error.getMessage(data, modelFile);
    } 
    ast = ast.ast;

    // Инициализируем окружение, в котором будут все типы модели
    // 
    for(var i = 0; i < ast.length; i++) {
        if (ast[i] instanceof meta.ASTImport) {
            loadModelAst(base, ast[i].getRealPath(base, ext), ext);
        } else if (ast[i] instanceof meta.ASTGroup) {
            var c = ast[i].classes;
            for(var j = 0; j < c.length; j++) {       
                result.addType(c[j].className, c[j]); // Добавляет только классы, для того чтобы иметь возможность получить список все классов
                ast[i].addType(c[j].className, c[j]);
            }
            result.addTypeGroup(ast[i].groupName, ast[i]);
        } else {
            result.addType(ast[i].className, ast[i]); // Добавляет только классы
            result.addTypeGroup(ast[i].className, ast[i]);
        }
    }    
}

function loadModel(modelFile, ext) {
    //
    result = meta.Environment;
    ext = ext || 'txt';
//    var dir = FilePath.dir(modelFile);
    var dir = path.dirname(modelFile);
    loadModelAst(dir, modelFile, ext);

    result.eachClass(function (c) {
        c.eachField(function (f) {
            if (f.type instanceof meta.ASTList) {
                f.type.get_content().addDependency(new meta.Dependency('OneToMany', f, c));
            }
            if (f.type instanceof meta.ASTSet) {
                f.type.get_content().addDependency(new meta.Dependency('ManyToMany', f, c));
            }
        });
    });

    return result.types;
}

function loadTemplate(context, templateFile) {
    var fname = findFile(templateFile, templateExtension);
    if (!fname) {
        throw "not found template '" + templateFile + "'";
    }
    var sample = new Template(fname, context);
    
    return sample;
}

function loadMap(mapFile) {
    var fname = findFile(mapFile, 'map.js');
    if (!fname) {
        throw "not found map '" + mapFile + "'";
    }
    return JSON.parse(fs.readFileSync(fname));
}

function time(prefix) {
    var date = new Date();
    console.log(prefix, date.getMinutes() + ':' + date.getSeconds() + "." + date.getMilliseconds());
}

exports.renderToString = renderToString;
exports.renderToFile = renderToFile;
exports.loadModel = loadModel;
exports.loadMap = loadMap;
exports.loadTemplate = loadTemplate;
exports.time = time;
exports.dirList = dirList;
exports.rawEncoding = rawEncoding;
