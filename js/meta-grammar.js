
var P = require('./jsparse.js')
    , ast = require('./meta-ast.js');

/* Граматика описания модели */
var MetaGrammar = (function () {

    function compress(r) {
        var result = [r[0]];
        for(var i = 0; i < r[1].length; i++) {
            result.push(r[1][i][1]);
        }
        return result;
    }

    var g = {};
    // Круглые скобки
    g.lparen = P.token('(');
    g.rparen = P.token(')');
    // 
    g.lbrace = P.token('{');
    g.rbrace = P.token('}');
    g.lbracket = P.token('[');
    g.rbracket = P.token(']');

    g.eql = P.token('=');
    g.to_lambda = P.token('->');
    g.left = P.token('<');
    g.right = P.token('>');
    g.semicolon = P.token(';');
    g.name = P.rtoken('[A-Za-z][\\w\\d_]*', '<ident>');

    g.groupKw = P.token('group');
    g.importKw = P.token('import');
    g.classKw = P.token('class');
    g.extendsKw = P.token('extends');
    g.glossaryKw = P.token('glossary');
    g.derivedKw = P.token('derived');
    g.inlineKw = P.token('inline');
    g.operationKw = P.token('operation');
    g.listKw = P.token('List');
    g.arrayKw = P.token('Array');
    g.setKw = P.token('Set');
    g.comma = P.token(',');
    g.dot = P.token('.');
    g.number = P.action(P.rtoken('\\d+(\\.\\d)*', '<number>'), function (r) { return parseFloat(r); });
    
    g.fieldDef = function(state) { return g.fieldDef(state); }
    g.typeDef = function(state) { return g.typeDef(state); }
    g.value = function(state) { return g.value(state); }

    g.string = P.action(P.rtoken('"[^"]*"', '<string>'), function (r) { return r.substring(1, r.length - 1); });

    g.typeList = P.action(P.wsequence(g.listKw, g.left, g.typeDef, g.right), function (r) {
        return new ast.ASTList(r[2]);
    });

    g.typeSet = P.action(P.wsequence(g.setKw, g.left, g.typeDef, g.right), function (r) {
        return new ast.ASTSet(r[2]);
    });

    g.typeDef = P.choice(g.typeList, g.typeSet, g.name);

    g.array = P.action(P.wsequence(g.lbracket, P.action(P.wsequence(g.value, P.repeat0(P.wsequence(g.comma, g.value))), compress), g.rbracket), function (r) {
        return r[1];
    });

    g.value = P.choice(g.number, g.name, g.string, g.array);

    g.defValue = P.action(P.wsequence(g.eql, g.value), function (r) {
        return r[1];
    });

    g.metaValue = P.action(P.wsequence(g.name, g.eql, g.value), function (r) {
        return new ast.ASTKeyValue(r[0], r[2]);
    });

    /* Метаданные */
    g.metaData = P.action(P.wsequence(g.lbracket, g.metaValue, P.repeat0(P.wsequence(g.comma, g.metaValue)), g.rbracket), function (r) {
        var result = {};
        result[r[1].keyName] = r[1];
        for(var i = 0; i < r[2].length; i++) {
            var p = r[2][i][1];
            result[p.keyName] = p;
        }
        return result;
    });

    g.expression = function(state) { return g.expression(state); }

    /* Выводимое свойство */
    g.arguments = P.action(P.wsequence(g.expression, P.repeat0(P.wsequence(g.comma, g.expression))), compress);

    g.call = P.action(P.wsequence(g.name, g.lparen, P.optional(g.arguments), g.rparen), function (r) {
        return new ast.ASTPCall(r[0], r[2]);
    });

    function toType(ast, typeCast) {
        return P.action(ast, function (r) {
            return new typeCast(r);
        });
    }

    g.expression = P.choice(g.call, toType(g.name, ast.ASTPVariable), toType(g.string, ast.ASTPString), toType(g.number, ast.ASTPNumber));

    g.derivedField = P.action(P.wsequence(g.derivedKw, g.typeDef, g.name, g.to_lambda, g.expression), function (r) {
        return new ast.ASTDerived(r[2], r[1], r[4]);
    });

    g.simpleField = P.action(P.wsequence(P.butnot(g.typeDef, g.derivedKw), g.name, P.optional(g.defValue)), function (r) {
        return new ast.ASTField(r[1], r[0], r[2])
    });

    /* Вложенный класс */
    g.inlineClass = P.action(P.wsequence(g.inlineKw, g.name, g.lbrace, P.repeat0(g.fieldDef), g.rbrace), function (r) {
        return new ast.ASTInline(r[1], r[3]);
    });

//    g.fieldDef = P.action(P.wsequence(optional(g.metaData), choice(g.inlineClass, P.wsequence(choice(g.derivedField, g.simpleField), g.semicolon))), function (r) {
//        r[1].metadata = r[0] || 'Nothing';
//        return r[1]; 
//    });

    g.fieldDef = P.action(P.wsequence(P.optional(g.metaData), P.wsequence(P.choice(g.derivedField, g.simpleField), g.semicolon)), function (r) {
        var r1 = r[1];
        r1[0].metadata = r[0] || 'Nothing';
        return r1[0]; 
    });

    g.base = P.optional(P.action(P.wsequence(g.extendsKw, g.name), function (r) { return r[1]; }));

    /* Обьявлние класса */    
    g.classDef = P.action(P.wsequence(P.optional(g.metaData), g.classKw, g.name, g.base, g.lbrace, P.repeat0(g.fieldDef), g.rbrace), function (r) {
        return new ast.ASTClass(r[2], r[3], r[5], r[0]);  
    });

    /* Обьявлние группы классов */    
    g.groupDef = P.action(P.wsequence(g.groupKw, g.name, g.lbrace, P.repeat0(g.classDef), g.rbrace), function (r) {
        return new ast.ASTGroup(r[1], r[3]);
    });

    /* Указание файла для импорта мругой модели */
    g.importPath = P.action(P.wsequence(g.name, P.repeat0(P.wsequence(g.dot, g.name))), function (r) {
        var result = [r[0]];
        for(var i = 0; i < r[1].length; i++) {
            result.push(r[1][i][0]);
            result.push(r[1][i][1]);
        }
        return result;
    });

    /* Импорт файла */
    g.importFile = P.action(P.wsequence(g.importKw, g.importPath, g.semicolon), function (r) {
        return new ast.ASTImport(r[1]);
    });

    /* */
    g.parser = P.action(P.wsequence(P.repeat0(P.choice(g.importFile, g.classDef, g.groupDef)), P.end_p), function (r) {
        return r[0]; 
    });

    return g;
} ());

exports.grammar = MetaGrammar;
