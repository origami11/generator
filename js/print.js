
function Constant(name) {
    return {name: name, toString: function () { return name; }}
}

function trim(str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function CodeInfo(name, code, prefix) {
    this.prefix = prefix;
    this.name = name;
    this.code = code;
    return code;
}

CodeInfo.prototype.isEmpty = function () {
    var s = this.code;
    return (typeof s == 'undefined') || s === null || s === false || (typeof s == 'string' && trim(s) == "") || (Array.isArray(s) && s.length == 0);
}

var NEWLINE = Constant('NEWLINE');
var OFFSET_SAVE = Constant('OFFSET_SAVE');
var OFFSET_RESTORE = Constant('OFFSET_RESTORE');

function repeat(length) {
    return Array(length + 1).join(" ");
}

// Вывод строки
function p_print(s, message, offset, left) {
    if (message.constructor === Array) {
//        console.log(message);
        // Вывод подстроки
        for(var i = 0; i < message.length; i++) {
            left = p_print(s, message[i], offset, left);
        }
    } else if (message.constructor === CodeInfo) {
//        s.push('<span class="' + message.prefix + '" data="' + message.name + '">');
        left = p_print(s, message.code, offset, left);
//        s.push('</span>');
    } else if (message === OFFSET_SAVE) {
        // Вывод со смещением
        offset.push(left + offset[offset.length - 1]);
        left = 0;
    } else if (message === OFFSET_RESTORE) {
        return offset.pop();
    } else if (message === NEWLINE) {
        left = 0;
        s.push("\r\n" + repeat(offset[offset.length - 1]));
//        s.push("\r\n");
    } else if (message.constructor === String || message.constructor === Number) {
        s.push(message);
        left += message.length;
    }

    return left;
}

function pretty_print(message) {
    var s = [];
    var offset = [0];
    p_print(s, message, offset, 0);
    return s.join("");
}

exports.trim = trim;
exports.NEWLINE = NEWLINE;
exports.OFFSET_SAVE = OFFSET_SAVE;
exports.OFFSET_RESTORE = OFFSET_RESTORE;
exports.repeat = repeat;
exports.pretty_print = pretty_print;
exports.CodeInfo = CodeInfo;
