// �������������� ������ ��� ����� �������� �������

var ast = require("./ast.js");

function MetaContext() {
    this.groups = {};
    this.types = {};

    /* ���� �� ��������� */
    this.addType('String', new ASTPrimitive('String'));
    this.addType('Url', new ASTPrimitive('Url'));
    this.addType('Email', new ASTPrimitive('Email'));
    this.addType('Code', new ASTPrimitive('Code'));
    this.addType('Text', new ASTPrimitive('Text'));
    this.addType('Article', new ASTPrimitive('Article'));
    this.addType('Integer', new ASTPrimitive('Integer'));
    this.addType('Number', new ASTPrimitive('Number'));
    this.addType('Image', new ASTPrimitive('Image'));
    this.addType('Boolean', new ASTPrimitive('Boolean'));
    this.addType('Time', new ASTPrimitive('Time'));
    this.addType('Date', new ASTPrimitive('Date'));
    this.addType('DateTime', new ASTPrimitive('DateTime'));
}

MetaContext.prototype = new ast.Context();

MetaContext.prototype.addType = function (name, type) {
    this.types[name] = type;
}

MetaContext.prototype.addTypeGroup = function (name, type) {
    this.groups[name] = type;
}

MetaContext.prototype.getType = function (name) {
    if (this.types.hasOwnProperty(name)) {
        return this.types[name];
    } else {
        return "Nothing";
    }
}

MetaContext.prototype.eachClass = function (fn) {
    for(var i in this.types) {
        if (this.types[i] instanceof ASTClass) {    
            fn(this.types[i]);
        }
    }
}

function Dependency(type, field, reference) {
    this.type = type;
    this.field = field; 
    this.reference = reference;
}

var Environment = new MetaContext();
var ClassActions = ['index', 'form', 'add', 'delete', 'search', 'results'];

function ASTImport(path) {
    this.path = path;
}

ASTImport.prototype.getRealPath = function (dir, ext) {
    return dir + "/" + this.path.join("/") + "." + ext;
}

/* ������ (�������� ��������� � ������) */
function ASTGroup(name, classes) {
    this.type = "Group";
    this.groupName = name;
    this.classes = classes;
    this.types = {};
}

ASTGroup.prototype.addType = function (name, type) {
    type.group = this;
    this.types[name] = type;
}

ASTGroup.prototype.get_first = function () {
    return this.classes[0];
}

function ASTOperation(name, type, metadata) {
    this.type = type;
    this.name = name;
    this.metadata = metadata || "Nothing"; 
}

/* ����� */
function ASTClass(name, superClass, fields, metadata) {
    // �������� ������ �� ������?
    this.system = Environment;
    this.type = "Class";
    this.className = name;
    this.superClass = superClass;
    this.operations = [];

    var self = this;
    fields.forEach(function (f) { f.parent = self; });

    this.fields = fields;
    this.group = "Nothing";
    this.actions = ClassActions;
    // ����������� ������ �� ������
    this.dependency = [];
    this.metadata = metadata || "Nothing"; // ���������� � ������
}


ASTClass.prototype.addOperation = function(name, type) {
    this.operations.push(new ASTOperation(name, type));
}

ASTClass.prototype.addDependency = function (d) {
    this.dependency.push(d);
}

ASTClass.prototype.get_adjective = function () {
    // ���������� ������ � ��������� ����� ���� ����������� ���� �� �����
    // ������ ��� ������ ���, ��� ������� ����� ���� �� ���!!!
    for(var i = 0; i < this.dependency.length; i++) {
        if (this.dependency[i].type == "OneToMany") return "True";
    }
    return "False";
}

ASTClass.prototype.eachField = function (fn) {
    this.fields.forEach(fn);
}

ASTClass.prototype.get_fields = function () {
    return this.fields;
}

ASTClass.prototype.get_superClass = function () {
    return Environment.getType(this.superClass);
}

ASTClass.prototype.get_first = function () {
    return this.fields[0];
}

ASTClass.prototype.toString = function () {
    return "class " + this.className + 
        ((this.superClass)? " extends " + this.superClass : "") + 
        " {\n" + this.fields + "\n}";
}

function ASTList(content) {
    this.type = "List";
    this.className = 'List';
    this.content = content;
}


ASTList.prototype.get_content = function () {
    return Environment.getType(this.content);
}

function ASTSet(content) {
    this.type = "List";
    this.className = 'Set';
    this.content = content;
}


ASTSet.prototype.get_content = function () {
    return Environment.getType(this.content);
}

function ASTPrimitive(name) {
    this.type = "Primitive";
    this.className = name;
}


ASTPrimitive.prototype.toString = function () {
    return '@' + this.className;
}

function ASTField(name, typedef, def, metadata) {
    this.fieldType = 'Simple';
    this.name = name;
    this.parent = null;
    this.type = typedef;
    this.value = def || "Nothing"; // �������� �� ���������
    this.metadata = metadata || "Nothing"; // ���������� � ����
    // ���������� ����������� � ����������� �� ����
    // �������� � ����������� ������ ���� ����
}


// ������ ���� ����� ���������������� ����� ���������� �������, ����� ���� ������.
// ����� ������� ����� ��� ��������� ����������
ASTField.prototype.get_dependency = function () {
    // ��� ���������� ������ ��� �� ���������� ����� ��� ��������� ����
    if (this.type instanceof ASTList) {
        return "OneToMany";
    } else if (this.type instanceof ASTSet) {
        return "ManyToMany";
    }
    return 'OneToOne';
}

ASTField.prototype.get_type = function () {
    if (typeof this.type == 'string') {
        return Environment.getType(this.type);
    }
    return this.type;
}

ASTField.prototype.toString = function () {
    return this.type + " " + this.name;
}

function ASTKeyValue(key, value) {
    this.keyName = key;
    this.value = value;        
}

function ASTMNumber (value) {
    this.value = value;
}

function ASTMString (value) {
    this.value = value;
}

function ASTMSymbol (value) {
    this.value = value;
}

function ASTDerived(name, typedef, expr) {    
    this.fieldType = 'Derived';
    this.parent = null;
    this.type = typedef;
    this.name = name;
    this.metadata = "Nothing";
    this.expr = expr;
}

ast.extend(ASTDerived, ASTField);

function ASTInline(name) {
    this.fieldType = 'Inline';
    this.parent = null;
    this.type = 'String';
    this.name = name;
    this.metadata = "Nothing";
}

ast.extend(ASTInline, ASTField);

ASTDerived.prototype.toString = function () {
    return 'derived ' + this.type + " " + this.name;
}

function ASTPCall(name, args) {
    this.type = "Call";
    this.name = name;
    this.args = args;
}

function ASTPVariable(name) {
    this.type = "Variable";
    this.name = name;
}

function ASTPString(value) {
    this.type = "String";
    this.value = value;
}


function ASTPNumber(value) {
    this.type = "Number";
    this.value = value;
}

function ASTArray(values) {
    this.data = values;
}

exports.Dependency = Dependency;
exports.Environment = Environment;
exports.ClassActions = ClassActions;
exports.ASTImport = ASTImport;
exports.ASTGroup = ASTGroup;
exports.ASTOperation = ASTOperation;
exports.ASTClass = ASTClass;
exports.ASTList = ASTList;
exports.ASTSet = ASTSet;
exports.ASTPrimitive = ASTPrimitive;
exports.ASTField = ASTField;
exports.ASTKeyValue = ASTKeyValue;
exports.ASTMNumber = ASTMNumber;
exports.ASTMString = ASTMString;
exports.ASTMSymbol = ASTMSymbol;
exports.ASTDerived = ASTDerived;
exports.ASTPCall = ASTPCall;
exports.ASTPVariable = ASTPVariable;
exports.ASTPString = ASTPString;
exports.ClassActions = ClassActions;
