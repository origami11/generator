function Template(self, template, map) {
    function makeTree(arr) {
        var node = null;
        if (arr[0].charAt(0) == ':') {
            node = map[arr[0]];
        } else {
            node = new Node(document.createElement(arr[0]));
        }
        var k = 1;
        
        if (arr.length > 1 && arr[1].constructor == Object) {
            var attr = arr[1];
            for(var i in attr) {
                if (i == '#') {
                    self[attr['#']] = node;
                } else {
                    node.attr(i, attr[i]);
                }
            }
            k = 2;
        }

        for(k; k < arr.length; k++) {
            if (arr[k].constructor == Array) {
                makeTree(arr[k]).appendTo(node);
            } else {
                node.html(arr[k]);
            }
        }
        return node;
    }

    return makeTree(template);
}

function NumberInput(attr) {
    this.attr = attr;
}

NumberInput.template = 
    ['span', {'class': 'input-number'},
        ['input', {'#': 'input'}],
        ['a', {'class': 'up', '#': 'up'}],
        ['a', {'class': 'down', '#': 'down'}]];

NumberInput.prototype.render = function () {
    var root = Template(this, NumberInput.template);

    function updateNumber(delta) {
        var val = parseInt(input.node.value);
        input.val(isNaN(val) ? 0 : val + delta * 1);
    }

    this.attr.type = 'text';
    var input = this.input.attr(this.attr).bind('keydown', function (e) {
        var kc = e.keyCode
        if (kc == 38) { updateNumber(1); }
        if (kc == 40) { updateNumber(-1); }
    });

    this.up.bind('click', function (e) {
        updateNumber(1);
    });
    this.down.bind('click', function (e) {
        updateNumber(-1);
    });

    return root;
}

function TimeInput(attr) {
    this.attr = attr || {};
    this.input = null;
    this.change = new Listener();
}

TimeInput.prototype.time = function (h, m) {
    if (m >= 60) {
        h++; m = 0;
    }
    if (m < 0) {
        h--; m = 59;
    }
    if (h < 0) h = 23;
    if (h > 23) h = 0;
    return Kit.align(h, 2) + ":" + Kit.align(m, 2);
}

TimeInput.prototype.setValue = function (v) {
    this.input.node.value  = v;
}

TimeInput.prototype.getValue = function (v) {
    return this.input.node.value;
}

TimeInput.template = 
    ['span', {'class': 'input-time'},
        ['input', {'#': 'input'}],
        ['a', {'class': 'up', '#': 'up'}],
        ['a', {'class': 'down', '#': 'down'}]];
    
TimeInput.prototype.render = function () {

    function updateTime(delta) {
        var val = input.node.value.split(":");
        var h = parseInt(val[0], 10);
        var m = parseInt(val[1], 10);
        if (isNaN(h)) h = 0;
        if (isNaN(m)) m = 0;

        var s = Kit.getSelectionStart(input.node);                      
        input.node.value = 
            (s >= 3) ? ((s >= 5) ? self.time(h, m + delta * 1) : self.time(h, m + delta * 10))
            : self.time(h + delta * 1, m);
        self.change.send(); 
        setSelectionRange(input.node, s, s);
    }

    var self = this;
    var root = Template(this, TimeInput.template);

    this.attr.type = 'text';
    var input = this.input.attr(this.attr).bind('keydown', function (e) {
        var kc = e.keyCode
        if (kc == 38) { updateTime(1); }
        if (kc == 40) { updateTime(-1); }
    }).bind('input', function () { 
        self.change.send(); 
    });

    this.up.bind('click', function (e) {
        updateTime(1);
    });

    this.down.bind('click', function (e) {
        updateTime(-1);
    });
    return root;
}



function DateInput(attr) {
    this.attr = attr || {};
    this.monthName = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    this.input = null;
    this.time = new Date();
    this.days = [];
    this.now  = new Date();
    this.selected = new Date();
    this.dayOfWeek = 0;
    this.daysInMonth = 0;
    this.dateName = null;
    this.change = new Listener();
}

DateInput.prototype.setValue = function (v) {
    this.input.node.value  = v;
}

DateInput.prototype.getValue = function (v) {
    return this.input.node.value;
}

DateInput.prototype.getDateName = function () {
    return this.monthName[this.time.getMonth()] +  ' ' + this.time.getFullYear();
}

DateInput.prototype.updateDays = function () {
    this.dateName.text(this.getDateName());
    var fullYear = this.time.getFullYear();
    var month = this.time.getMonth();

    var first = new Date(fullYear, month, 1);
    var last = new Date(fullYear, month + 1, 1);

    var days = this.daysInMonth = Math.ceil((last - first) / 1000 / 60 / 60 / 24);
    var day = this.dayOfWeek = (first.getDay() == 0) ? 7 : first.getDay();

    for(var i = 0; i < this.days.length; i++) {
        this.days[i].node.style.backgroundColor = '';
        this.days[i].node.style.fontWeight = 'normal';
        if (i >= (day - 1) && i < day - 1 + days) {
            if (this.now.getFullYear() == fullYear && this.now.getMonth() == month && this.now.getDate() == (i - day + 2)) {
                this.days[i].node.style.backgroundColor = '#ffdfdf';
            } 
            if (this.selected.getFullYear() == fullYear && this.selected.getMonth() == month && this.selected.getDate() == (i - day + 2)) {
                this.days[i].node.style.fontWeight = 'bold';
            }       
            this.days[i].text(i - day + 2);
        } else {
            this.days[i].text('');
        }
    }
}

DateInput.prototype.setTime = function(time) {
    this.time = time;
    this.updateDays();
}

DateInput.prototype.render = function () {
    var self = this;
    var root = node('span', 'input-date').bind('click', function (e) { 
        Kit.stopBubble(e);
    });

    var body = new Node(document.body)
    var data = node('div', 'container').appendTo(body);    
    var top = new Node(window);
    this.attr.type = 'text';
    var input = this.input = node('input').attr(this.attr).bind('focus', function (e) {
        data.show();
        data.relativeTo(this, 0, this.offsetHeight - 1);

        var n = input.node.value.split("-");
        if (n.length == 3) {
            var year = parseInt(n[0], 10);
            var month = parseInt(n[1], 10);
            var day = parseInt(n[2], 10);
            if (!isNaN(year) && !isNaN(month) && !isNaN(day)) {
                self.selected = self.time = new Date(year, month - 1, day);
                self.updateDays();
            }
        }
        top.once('click', function (e) { 
            data.hide(); 
        });
        return false;
    }).bind('input', function () {
        self.change.send(); 
    }).appendTo(root);

    var nav = node('div', 'navigate').appendTo(data);

    node('span', 'push next').html('&raquo;').bind('click', function (e) {
        Kit.stopBubble(e);
        self.setTime(new Date(self.time.getFullYear() + 1, self.time.getMonth(), 1));
    }).appendTo(nav);
    node('span', 'push next').html('&rsaquo;').bind('click', function (e) {
        Kit.stopBubble(e);
        self.setTime(new Date(self.time.getFullYear(), self.time.getMonth() + 1, 1));
    }).appendTo(nav);

    node('span', 'push prev').html('&laquo;').bind('click', function (e) {
        Kit.stopBubble(e);
        self.setTime(new Date(self.time.getFullYear() - 1, self.time.getMonth(), 1));
    }).appendTo(nav);
    node('span', 'push prev').html('&lsaquo;').bind('click', function (e) {
        Kit.stopBubble(e);
        self.setTime(new Date(self.time.getFullYear(), self.time.getMonth() - 1, 1));
    }).appendTo(nav);

    this.dateName = node('span', 'date-name').appendTo(nav);

    var table = node('table').appendTo(data);
    node('tbody').repeat(6, function (i) {
        node('tr').repeat(7, function (j) {
            var n = node('td').appendTo(this);
            if (j == 7) n.node.className = 'weekend';
            self.days.push(n);
        }).appendTo(this);
    }).appendTo(table);

    this.days.forEach(function (e, i) {
        e.bind('click', function (e) {
            var days = self.daysInMonth;
            var day  = self.dayOfWeek;
            if (i >= (day - 1) && i < day - 1 + days) {
                var n = i - day + 2;
                input.node.value = self.time.getFullYear() + '-' + Kit.align(self.time.getMonth() + 1, 2) + '-' + Kit.align(n, 2);
                self.selected = new Date(self.time.getFullYear(), self.time.getMonth(), n);
                self.updateDays();
                data.hide();
                self.change.send(); 
            }
            return false;
        });
    });

    this.updateDays();

    return root;
}

function DateTimeInput(attr) {
    this.attr = attr || {};
    this.date = new DateInput();
    this.time = new TimeInput();
    this.input = null;
}

DateTimeInput.prototype.setValue = function (val) {
    var index = val.indexOf('T');
    
    if (index >= 0) {
        this.date.setValue(val.substring(0, index));
        this.time.setValue(val.substring(index+1, val.length-1));
    }

    this.input.node.value = val;
}

DateTimeInput.prototype.render = function () {
    var root = node('span', 'input-datetime');
    var input = this.input = node('input').attr(this.attr).appendTo(root);
    input.node.setAttribute('type', 'hidden');
    var self = this;

    this.date.render().appendTo(root);
    this.time.render().appendTo(root);
    node('a', 'datetime-now').text('сейчас').bind('click', function (e) {
        self.setValue(Kit.dateFormat(new Date()));
    }).appendTo(root);

    this.setValue(input.node.value);

    this.date.change.next(function (e) {
        input.node.value = self.date.getValue() + 'T' + self.time.getValue() + 'Z';
    });

    this.time.change.next(function (e) {
        input.node.value = self.date.getValue() + 'T' + self.time.getValue() + 'Z';
    }); 
    return root;
}

function replaceTo(name, _class) {
    var el = Kit.getById(name);
    var n = el.node;
    var o = new (_class)({name: n.getAttribute('name'), id: n.id, value: n.value});
    el.replace(o.render());
}

function Select(attr, items) {
    this.attr = attr || {};
    this.items = items;
    this.value = null;
    this.input = null;
}

Select.prototype.setSelected = function (el) {
    this.value.text(el.text);
    this.input.node.value = el.value;

    for(var i = 0; i < this.items.length; i++) {
        var s = this.items[i].selected = this.items[i] == el;
        this.items[i].node.node.style.fontWeight = s ? 'bold' : 'normal';
    }        
}

Select.prototype.render = function () {
    var selected = getSelected(this.items);

    var root = node('span', 'input-select');
    var top = new Node(window);
    var value = this.value = node('span', 'input-value').bind('click', function (e) {
        Kit.stopBubble(e);
        list.show();                
        list.relativeTo(this, 0, this.offsetHeight - 1);

        top.once('click', function (e) { 
            list.hide(); 
        });
    }).appendTo(root);

    var self = this;

    var list = node('div', 'select-options').appendTo(new Node(document.body));
    var input = this.input = node('input').attr(this.attr).appendTo(root);
    input.attr('type', 'hidden');

    Kit.each(this.items, function (el, i) {        
        el.node = node('a', 'line').text(el.text).bind('click', function (e) {
            self.setSelected(el);
            list.hide();
        }).appendTo(list);
    });

    if (selected.length > 0) {
        this.setSelected(selected[0]);
    }
    
    return root;
}

function getSelected(options) {
    var result = [];
    for(var i = 0; i < options.length; i++) {
        if (options[i].selected) result.push(options[i]);
    }
    return result;
}

/* Страницы */
function Pages() {
    this.pages = [];
    this.range = 5;
    this.page = 1;
    this.count = 10;
    this.change = new Listener();
}

Pages.prototype.render = function () {
    var self = this;
    var pages = this.pages;
    var root = node('div', 'pages');
    node('a').html('&laquo;').bind('click', function (e) {
        self.change.send(1);        
    }).appendTo(root);
    node('a').html('&lsaquo;').bind('click', function (e) {
        self.change.send(Math.max(self.page - 1, 1));        
    }).appendTo(root);

    root.repeat(this.range * 2 + 2, function (i) {
        pages.push(node('a').text(i).bind('click', function (e) {            
            self.change.send(Math.max(self.page - self.range, 1) + i - 1);
        }).appendTo(root));
    });

    node('a').html('&rsaquo;').bind('click', function (e) {
        self.change.send(Math.min(self.count, self.page + 1));
    }).appendTo(root);

    node('a').html('&raquo;').bind('click', function (e) {
        self.change.send(self.count);        
    }).appendTo(root);

    return root;
}

Pages.prototype.setPage = function (page, pages) {
    this.page = page;
    this.count = pages;
    var range = this.range;
    var k = 0;
    for (var i = Math.max(page - range, 1); i <= Math.min(pages, page + range); i++) {
        var e = this.pages[k];
        if (page == i) { e.addClass('active'); } else { e.removeClass('active'); }
        e.show().text(i);
        k++;
    };
    var N = (this.range * 2 + 1);
    for(var i = k; i <= N; i++) {
        this.pages[i].hide();
    }
}

function SelectPages(attr, items) {
    this.attr = attr;
    this.items = items;
    this.value = null;
    this.input = null;
}

SelectPages.prototype.setSelected = function (el) {
    this.value.text(el.text);
    this.input.node.value = el.value;

    for(var i = 0; i < this.items.length; i++) {
        var s = this.items[i].selected = this.items[i] == el;
        this.items[i].node.node.style.fontWeight = s ? 'bold' : 'normal';
    }        
}

SelectPages.prototype.fillItems = function (root, list) {
    var self = this;
    // Заполянем список
    Kit.each(this.items, function (el, i) {
        el.node = node('a', 'line').text(el.text).bind('click', function (e) {
            self.setSelected(el);
            list.hide();
        }).appendTo(root);
        if (el.selected) {
            el.node.node.style.fontWeight = 'bold';
        }
    });
}

SelectPages.template = 
    ['span', {'class': 'input-pages'},
        ['span', {'class': 'input-select'},
            ['span', {'class': 'input-value', '#': 'value'}],
            ['input', {'#': 'input'}]
        ],
        ['a', {'class': 'input-clear', '#': 'clear'}]
    ];

SelectPages.templateList = 
    ['div', {'class': 'select-pages'},
        [':pages'], /* Место для подстановки значения. Составной элемент. Вместо createElement */
        ['div', {'class': 'select-values', '#': 'values'}]
    ];

SelectPages.prototype.render = function () {
    var selected = getSelected(this.items);

    var spages = Template(this, SelectPages.template);
    var top = new Node(window);
    var value = this.value.bind('click', function (e) {
        Kit.stopBubble(e);
        list.show();
        list.relativeTo(this, 0, this.offsetHeight - 1);

        top.once('click', function (e) { 
            list.hide(); 
        });
    });

    this.clear.bind('click', function (e) {
        self.value.text("Не выбрано");
        self.input.node.value = "";        
    });

    var self = this;

    // Страницы
    var pages = new Pages();
    var proot = pages.render().bind('click', Kit.stopBubble);

    var list = Template(this, SelectPages.templateList, {':pages': proot}).appendTo(new Node(document.body));
    this.input.attr(this.attr).attr('type', 'hidden');

    pages.change.next(function (page) {
        // Выбрать по запросу
        Node.getJSON(self.attr.provider, {page: page}, function (data) {
            if (data.count == 1) proot.hide();
            pages.setPage(page, data.count);
            var items = data.data;
            if (data.length != self.items.length) {
                self.items = new Array(data.length);
            }

            for(var i = 0; i < items.length; i++) {
                self.items[i] = {text: items[i].value, value: items[i].key, selected: self.input.node.value == items[i].key};
            }   
            
            // Потом сделать чтобы не нужно было очищать, а заново использовать теже элементы
            self.values.clean();
            self.fillItems(self.values, list);
        });
    });

    self.fillItems(self.values, list);
    pages.change.send(1);

    if (selected.length > 0) {
        this.setSelected(selected[0]);
    }
    
    return spages;
}

function SelectMultiPages(attr, items) {
    this.attr = attr;
    this.items = items;
    this.values = null;
    this.input = null;
}

SelectMultiPages.prototype.setSelected = function (el) {
    var value = node('span', 'input-value').text(el.text).appendTo(this.values);
    node('input').attr({type: 'hidden', name: this.attr.name}).val(el.value).appendTo(value);

    node('a', 'input-clear').bind('click', function (e) { 
        value.remove();
    }).appendTo(value);


    for(var i = 0; i < this.items.length; i++) {
        var s = this.items[i].selected = this.items[i] == el;
        this.items[i].node.node.style.fontWeight = s ? 'bold' : 'normal';
    }        
}

SelectMultiPages.prototype.fillItems = function (root, list) {
    var self = this;
    // Заполянем список
    Kit.each(this.items, function (el, i) {
        el.node = node('a', 'line').text(el.text).bind('click', function (e) {
            self.setSelected(el);
            list.hide();
        }).appendTo(root);
        if (el.selected) {
            el.node.node.style.fontWeight = 'bold';
        }
    });
}

SelectMultiPages.prototype.render = function () {    
    var selected = getSelected(this.items);

    var spages = node('span', 'data');
    var values = this.values = node('span', 'values').appendTo(spages);
    var badd = node('a', 'add-button').text('добавить').appendTo(spages);
    var top = new Node(window);

    var list = node('div', 'select-pages').appendTo(new Node(document.body));
    var pages = new Pages();
    var proot = pages.render().bind('click', Kit.stopBubble).appendTo(list);
    var values = node('div', 'select-values').appendTo(list);

    badd.bind('click', function (e) {
        Kit.stopBubble(e);
        list.show();
        list.relativeTo(this, 0, this.offsetHeight - 1);

        top.once('click', function (e) { 
            list.hide(); 
        });        
    });

    var self = this;
    pages.change.next(function (page) {
        // Выбрать по запросу
        Node.getJSON(self.attr.provider, {page: page}, function (data) {
            if (data.count == 1) proot.hide();
            pages.setPage(page, data.count);
            var items = data.data;
            if (data.length != self.items.length) {
                self.items = new Array(data.length);
            }

            for(var i = 0; i < items.length; i++) {
                self.items[i] = {text: items[i].value, value: items[i].key, selected: /*self.input.node.value == items[i].key*/ false};
            }   
            
            // Потом сделать чтобы не нужно было очищать, а заново использовать теже элементы
            values.clean();
            self.fillItems(values, list);
        });
    });

    self.fillItems(values, list);
    pages.change.send(1);
    for(var i = 0; i < selected.length; i++) {
        this.setSelected(selected[i]);
    }

    return spages;
}

function replaceSelect(name, _class) {
    var el = Kit.getById(name);
    var n = el.node;
    var options = [];
    for(var i = 0; i < n.options.length; i++) {
        options.push({text: n.options[i].text, value: n.options[i].value, selected: n.options[i].selected});
    }
    
    var o = new (_class)({name: n.getAttribute('name'), id: n.id, value: n.value, provider: n.getAttribute('data-provider')}, options);
    el.replace(o.render());
}
