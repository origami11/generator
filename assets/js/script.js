
window.onload = function () {
    Kit.getAll('.dropmenu').each(Visualize.dropMenu);
    Kit.getAll('.list-image-preview').each(Visualize.imagePreview);

    var checkbox = Kit.getAll('input.list-select').each(Visualize.checkBox);
    var table = Kit.getById('list-table');
    if (table.node) {
        var menu = Kit.getById('operation');
        var provider = table.node.getAttribute('data-provider');
        menu.display(checkbox.some(function (c) { return c.checked; }));
        checkbox.bind('mouseup', function () {
            Node.getJSON(provider, {checked: this.checked, value: this.value}, function (result) {  
                menu.display(result.count > 0);
            });
        });    
    }

    Kit.getAll('.group>.name').bind('click',function () {
        Kit.toggleClass(this.parentNode, 'hide');
    });

    var list = Kit.getAll('.table-name');
    list.bind('click', function () {
        list.each(function (item, i) {
            var forid = document.getElementById(item.getAttribute('data-for'));
            forid.style.display = (item.checked) ? 'block' : 'none';
        });
    });
};