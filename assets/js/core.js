/**/

Kit = {
    addHandler: function (object, event, handler) {
        if (typeof object.addEventListener != 'undefined')
            object.addEventListener(event, handler, false);
        else if (typeof object.attachEvent != 'undefined')
            object.attachEvent('on' + event, handler);
    },

    removeHandler: function (object, event, handler) {
        if (typeof object.removeEventListener != 'undefined')
            object.removeEventListener(event, handler, false);
        else if (typeof object.detachEvent != 'undefined')
            object.detachEvent('on' + event, handler);
    },

    dispatchEvent: function (o, name) {
        if(document.createEvent) {
            var o = document.createEvent('MouseEvents');
            o.initEvent(name, true, false);
            element.dispatchEvent(o);
        } else if( document.createEventObject ) {
            o.fireEvent(name);
        }
    },

    getElementDefaultDisplay: function (t) {
        var cStyle, gcs = "getComputedStyle" in window;

        cStyle = (gcs ? window.getComputedStyle(t, "") : t.currentStyle).display; 
        return cStyle;
    },

    getX: function( oElement ) {
        var iReturnValue = 0;
        while( oElement != null ) {
            iReturnValue += oElement.offsetLeft;
            oElement = oElement.offsetParent;
        }
        return iReturnValue;
    },

    getY: function ( oElement ) {
        var iReturnValue = 0;
        while( oElement != null ) {
            iReturnValue += oElement.offsetTop;
            oElement = oElement.offsetParent;
        }
        return iReturnValue;
    },

    addClass: function(node, name) {
        if (!Kit.hasClass(node, name)) {
            node.className += ' ' + name;
        }
    },

    hasClass: function(node, name) {
        return node.className.indexOf(name) >= 0;
    },        

    toggleClass: function(node, className) {
        if (Kit.hasClass(node, className)) {
            Kit.removeClass(node, className);
        } else {
            Kit.addClass(node, className);
        }
    },

    removeClass: function(node, name) {
        node.className = node.className.replace(name, '');
    },

    dateFormat: function(date) {
        return date.getFullYear() + '-' + Kit.align(date.getMonth() + 1, 2) + '-' + Kit.align(date.getDate(), 2) + 
            'T' + Kit.align(date.getHours(), 2) + ':' + Kit.align(date.getMinutes(), 2) + 'Z';
    },

    stopBubble: function(oEvent) {
        if(oEvent && oEvent.stopPropagation) {
            oEvent.stopPropagation();       // ��� DOM-ᮢ���⨬�� ��㧥஢
        } else {
            window.event.cancelBubble = true; //��� IE
        }
    },

    align: function(s, n) {
        return Array(n + 1 - (s).toString().length).join("0") + s;
    },

    each: function(list, fn) {
        for(var i = 0; i < list.length; i++) {        
            fn(list[i], i);
        }
    },

    getSelectionStart: function(o) {
        if (o.createTextRange) {
            var r = document.selection.createRange().duplicate()
    		r.moveEnd('character', o.value.length)
    		if (r.text == '') return o.value.length
    		return o.value.lastIndexOf(r.text)
    	} else return o.selectionStart
    },

    setSelectionRange: function(input, selectionStart, selectionEnd) {
        if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        } else if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        }
    },

    getById: function(id) {
        return new Node(document.getElementById(id));
    },

    getAll: function(selector) {
        return new NodeList(document.querySelectorAll(selector));
    },

    extend: function (Child, Parent) {
    	var F = function () {}
    	F.prototype = Parent.prototype;
    	Child.prototype = new F();
    	Child.prototype.constructor = Child;
    	Child.superClass = Parent.prototype;
    },
    
    makeClass: function () {
        var Parent = false, fn;
        if (arguments.length == 1) {
            fn = arguments[0];
        }
        if (arguments.length == 2) {
            Parent = arguments[0];
            fn = arguments[1];
        }
        var F = function () {
            this.construct.apply(this, arguments);
        };
        if (Parent) { Kit.extend(F, Parent); }
        fn(F.prototype);
        return F;
    }
};

function Listener (root) {
    this.root = root;
    this.list = [];
}

Listener.prototype = {
    next: function (fn) {
        this.list.push(fn);
    },

    remove: function (fn) {
        var index = this.list.indexOf(fn);

        if (index >= 0) {
            this.list.splice(index, 1);
        }
    },

    send: function () {
        for(var i = 0; i < this.list.length; i++) {
            this.list[i].apply(this.root, arguments);
        }
    }
};

var NativeListener = Kit.makeClass(function (P) {
    P.construct = function(name, target) {
        this.name = name;
        this.target = target;
    }

    P.next = function (f) {
        Kit.addHandler(this.target, this.name, f);
    }

    P.remove = function (f) {
        Kit.removeHandler(this.target, this.name, f);
    }
});

function Node(node) {
    this.node = node;
}

function node(name, className) {
    var result = document.createElement(name);
    if (className) result.className = className;
    return new Node(result);
}

Node.prototype = {
    appendTo: function(to) {
        to.node.appendChild(this.node);
        return this;
    },

    hide: function() {
        this.node.style.display = 'none';
        return this;
    },

    show: function(type) {    
        type = type || 'inline';
        this.node.style.display = type;
        return this;
    },

    display: function (flag) {
        if (flag) { this.show(); } else { this.hide(); };
        return this;
    },

    attr: function() {
        if (arguments.length == 2) {            
            this.node.setAttribute(arguments[0], arguments[1]);
        } else {
            var attrs = arguments[0];
            for(var i in attrs) {
                if (attrs.hasOwnProperty(i)) this.node.setAttribute(i, attrs[i]);
            }
        }
        return this;
    },

    css: function(attrs) {
        for(var i in attrs) {
            this.node.style[i] = attrs[i];
        }
        return this;
    },

    position: function(x, y) {
        this.node.style.left = Math.round(x) + 'px';
        this.node.style.top = Math.round(y) + 'px';
        return this;
    },

    size: function(w, h) {
        this.node.style.width = Math.round(w) + 'px';
        this.node.style.height = Math.round(h) + 'px';
        return this;
    },

    val: function(value) {
        this.node.value = value;
        return this;
    },

    html: function(text) {
        this.node.innerHTML = text;
        return this;
    },

    text: function(text) {
        if (typeof this.node.textContent == 'undefined') {
            this.node.innerText = text;
        } else {
            this.node.textContent = text;
        }
        return this;
    },

    replace: function(to) {
        this.node.parentNode.replaceChild(to.node, this.node);
        return this;
    },

    bind: function (event, fn) {
        Kit.addHandler(this.node, event, fn);
        return this;
    },

    remove: function () {
        this.node.parentNode.removeChild(this.node);
        return this;
    },

    repeat: function (n, fn) {
        for(var i = 0; i < n; i++) {
            fn.call(this, i + 1);
        }
        return this;
    },

    relativeTo: function (el, ox, oy) {
        this.node.style.left = Kit.getX(el) + ox + 'px';
        this.node.style.top = Kit.getY(el) + oy + 'px';
    },

    addClass: function (name) {
        Kit.addClass(this.node, name);
        return this;
    },

    removeClass: function (name) {
        Kit.removeClass(this.node, name);
        return this;
    },

    clean: function () {
        while(this.node.firstChild) {
            this.node.removeChild(this.node.firstChild);
        }
    },

    once: function (name, fn) {
        var callback = function (e) {
            fn(e);
            Kit.removeHandler(this.node, name, callback);
        }
        Kit.addHandler(this.node, name, callback);
    }
}

Node.from = function (e) {
    return new Node(e);
}

Node.make = function (name, className) {
    return node(name, className);
}

Node.getJSON = function (url, args, callback) {
    Node.getText(url, args, function (text) {
        var result = eval("(" + text + ")");
        callback(result);
    });
}

Node.getText = function (url, args, callback) {
    var http = new XMLHttpRequest();
    var params = [];
    for(var i in args) {
        params.push(i + "=" + encodeURIComponent(args[i]));
    }
    if (url.indexOf('?') >= 0) {
        http.open("GET", url + "&" + params.join('&'), true);
    } else {
        http.open("GET", url + "?" + params.join('&'), true);
    }
    http.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                callback.call(this, this.responseText);
            } 
        }
    };

    http.send(); 
}


function NodeList(list) {
    this.list = list;
}

NodeList.prototype = {
    each: function (fn) {
        Kit.each(this.list, fn);
        return this;
    },

    bind: function (event, fn) {
        Kit.each(this.list, function (item) {
            Kit.addHandler(item, event, fn);
        });
        return this;
    },

    some: function (fn) {
        for (var i = 0; i < this.list.length; i++) {
            if (fn(this.list[i], i)) return true;
        }
        return false;
    },
    
    every: function (fn) {
    }
};
