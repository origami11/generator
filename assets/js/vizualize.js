/* */
Visualize = {};

(function () {
    Visualize.dropMenu = function (menu) {
        var over = false;
        var parent = new Node(menu.parentNode);
        parent.bind('mouseout', function () {
            var self = this;
            over = false;
            setTimeout(function () { 
                if (!over) self.className = ''; 
            }, 100);
        }).bind('mouseover', function () {
            over = true;
        }).bind('click', function () {
            this.className = 'active';
            menu.style.left = Kit.getX(this) + 'px';
            menu.style.top = Kit.getY(this) + this.offsetHeight + 'px';
        });
    }
} ());

(function () {
    var checkboxHeight = 14;
    function change() {
        element = this.nextSibling;
        if(element.checked == true && element.type == "checkbox") {
            this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
        } else if(element.type == "checkbox") {
            this.style.backgroundPosition = "0 0";
        }
    }

    function pushed() {
    }

    function check() {
        element = this.nextSibling;
        if(element.checked == true && element.type == "checkbox") {
            this.style.backgroundPosition = "0 0";
            element.checked = false;
        } else if(element.type == "checkbox") {
            this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
            element.checked = true;
        }
        Kit.dispatchEvent(element, 'mouseup');
    }

    Visualize.checkBox = function (input) {
        var span = node("span", input.type);
        input.parentNode.insertBefore(span.node, input);
        Kit.addHandler(input, 'change', change);
        span.bind('mousedown', pushed).bind('mouseup', check);

        change.call(span.node);
    }
} ());


(function () {
    function updatePosition(element, x, y) {
        element.style.display = 'block';
        element.style.left = (x - element.offsetWidth) + 'px';
        element.style.top = y + 'px';        
    }

    Visualize.imagePreview = function (link) {
        var img = document.createElement("img");
        var x, y;
        
        img.onload = function () {
            updatePosition(this, x, y);
        };

        img.className = 'preview';
        link.parentNode.insertBefore(img, link);
        link.onclick = function () {
            return false;
        }
                        
        link.onmouseover = function () {
            x = Kit.getX(this);
            y = Kit.getY(this);
            img.src = this.href;
        }
        
        link.onmouseout = function () {
            img.style.display = 'none';
        }
    }
} ());
