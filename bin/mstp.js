// Обработка модели
// Параметры 
// require.paths.push('..');

var include_path = __dirname + "/../js/";
var opt = require(include_path + "options.js");

var schema = [
    ['p',  'model',   '!:',  'Model file'],
    ['t',  'template', '!:',  'Template file'],
    ['m',  'map', '!:',  'Map file'],
    ['o',  'output',   ':',  'Output path'],
    ['c',  'codepage', ':',  'Codepage (cp1251)'],
    ['g',  'groups', '',  'Use groups']
];

var result = opt.parseOptions(schema);
console.log(result.options);

var templateName = result.options.template;
var mapName = result.options.map;
var project_name = result.options.model;

var output_path = __dirname + "/../apps/" + project_name + "/";    
if (result.options.hasOwnProperty('output')) {
    output_path = result.options.output;
}

var ast = require(include_path + "ast.js")
    , meta = require(include_path + "meta-ast.js")
    , tpl = require(include_path + "template.js");

var convert = tpl.rawEncoding;
if (result.options.hasOwnProperty('codepage')) {
    convert = require(include_path + result.options.codepage + ".js").convert;
}

var e = meta.Environment;
var model = tpl.loadModel(project_name + '.txt');

ast.initDefaultMethods(e);

tpl.dirList.push(__dirname + "/../templates/");
tpl.loadTemplate(e, templateName);

e.setMap(tpl.loadMap(mapName));

if (result.options.hasOwnProperty('groups')) {
    e.addProduction('entry', e);
} else {
    e.addProduction('entry', model);
}


e.product.next(function (context, entry, data, file) {
    tpl.renderToFile(context, entry, data, output_path + file, convert);
});

e.render();
