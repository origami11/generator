
var args = process.argv.splice(2);
if (args.length < 1) {
    console.log("using: cstp template");
} else {

    var make = 'make.js', data = null;
    var template = args[0], map = 'html';

    if (args.length > 1) {
        map = args[1];
    }        

    var include_path = "../js/";

    var ast = require(include_path + "ast.js")
        , tpl = require(include_path + "template.js")
        , fs = require("fs")
        , path = require("path");

    var e = new ast.Context();
    ast.initDefaultMethods(e);

    if (path.existsSync(make)) {
        var fileData = fs.readFileSync(make, 'utf-8');
        data = JSON.parse(fileData);
    } else {
        data = [{name: "index"}];
    }

    tpl.dirList.push(__dirname + "/../templates/utils/");
    tpl.loadTemplate(e, template);   
    e.setMap(tpl.loadMap(map));
    e.addProduction('entry', data);

    e.product.next(function (context, entry, data, file) {
        // console.log(file);
        tpl.renderToFile(context, entry, data, file);
    });                                                                         

    e.render();
}
