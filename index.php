<?php ini_set('default_charset', "utf-8"); ?>
<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style type="text/css">
body, html {padding: 100px 0 0 0; margin: 0px; background: #efefef;}
#app-list {width: 900px; margin: 0 auto; text-align: center;}
#app-list ul {list-style-type: none; margin: 0px; padding: 0px;}
#app-list li { border: #cfcfcf 1px solid; text-align: center; float: left; margin-right: 10px; margin-top: 10px; border-radius: 10px; width: 200px; padding: 50px 0; background: #fff; }
#app-list a {text-decoration: none; font-family: Calibri; font-size: 30px; font-weight: bold; color: #555}
#app-list li:hover {border: #227bb3 1px solid ; }
#app-list a:hover {color: #227bb3; }
</style>
</head>
<body>
<div id="app-list">
<ul> 
<?php

$list = file('apps/Descript.ion');

foreach($list as $app) {
    $name = explode(" ", $app, 2);
    echo  "<li><a href='apps/{$name[0]}'>", iconv('cp866', 'utf-8', $name[1]), "</a></li>";
}

?>
</ul>
</div>
</body>
</html>