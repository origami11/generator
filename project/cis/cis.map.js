{
    "entry": "index.php",
    "bootPage": "classes/boot.php",
    "index": "classes/view/index.php",

    "classModel": "classes/query/{className:lowercase}.php",
    "controller": "classes/controller/{className:lowercase}.php",

    "reportPage": "classes/view/{group.groupName:lowercase}/{className:lowercase}_view.php",
    "searchPage": "classes/view/{group.groupName:lowercase}/{className:lowercase}_search.php",
    "renderListPage": "classes/view/{group.groupName:lowercase}/{className:lowercase}_list.php",
    "renderEditPage": "classes/view/{group.groupName:lowercase}/{className:lowercase}_form.php"
}