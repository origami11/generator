<?php

class Meetings_Ext {
    public function getUrlFile($row) {
        return "http://www.edu.yar.ru/sir/get.php?file={$row['file_path']}&fms=1&do=get&name={$row['file_name']}&type={$row['file_type']}";
    }

    public function getUrlLogo($row) {
        return "http://fms.edu.yar.ru/webunicom/web/meeting/{$row['narrator']}/l{$row['id_logo']}.jpg";
    }

    public function getUrlPresentation($row) {
        return "http://fms.edu.yar.ru/webunicom/web/meeting/{$row['narrator']}/p{$row['id_presentation']}/data.ppt";
    }

    public function getUrlWebtour($row) {
        return "http://fms.edu.yar.ru/webunicom/web/meeting/{$row['narrator']}/t{$row['id_webtour']}/data.csv";
    }
}