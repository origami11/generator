group Arrangement {
    [tableName = projects, label = "Проекты", library = main]
    class Project {
        [label = "Название"] String name;
        [label = "Префикс"]  String pref;
        [label = "Описание", display = None]
            Text description;
        [label = "Почта"]    Email email;
        [label = "Руковолитель", display = None]
            String guide;
        [label = "Год"]      Integer year;
        [label = "Ссылка", display = None]
            Url link;
            
        [label = "Статус", enum = StatusProject]
            Integer status;

        [display = None] Integer email_request;
        [display = None] Integer email_answer;

        [listKey = id_project]
            List<Player> players;
        [listKey = id_project]
            List<Turn> turns;
    }

    [tableName = p_organizations, label = "Организации", indexName = id_org]
    class Project_Organization {
        [label = "Название"] String name;
        [label = "Адрес", display = None]
            Text address;
        [label = "Телефон"]  String phones;
        [label = "Директор"] String director;
        [label = "Почта"]    Email email;
        [label = "Создана"]  Date date;
    }

    [tableName = p_turns, label = "Туры"]
    class Turn {
        [label = "Название"] String name;
        [label = "Описание", display = None]
            Text description;
        [label = "Открытие"]          DateTime date_open;
        [label = "Регистрация до"]    DateTime date_reg;
        [label = "Начало"]            DateTime date_begin;
        [label = "Конец"]             DateTime date_end;
        [label = "Публикация итогов"] DateTime date_res;
    }

    [tableName = p_players, label = "Участники"]
    class Player {
        [label = "Индекс"] Integer amount;
        [label = "Имя"] String name;
        [label = "Статус"] Integer status;
        [label = "Руководитель", display = None]
            String guide;
        [label = "Почта"] Email email;
        [label = "Пароль", display = None]
            String delivery;
        Project_Organization id_org;

        [listKey = id_player, display = Existence]
            List<Story> story;

        [listKey = id_player, display = Existence]
            List<Player_Image> images;

        [listKey = id_player, display = Existence]
            List<Player_Video> videos;
    }

    [tableName = p_story, indexName = id_story, label = "Репортаж"]
    class Story {
        [label = "Заголовок"] String title;
        [label = "Описание"] String description;
        [label = "Тип"] String type;
        Form id_form;

        Player_Image id_image;

        [tableMap = p_story_images, listKey = id_image, selfKey = id_story]
        Set<Player_Image> images;
    }

    [tableName = pi_videos, indexName = id_video, display = Existence, label = "Видео"]
    class Player_Video {
        [label = "Заголовок"] String name;
        [label = "Описание"] String description;
        [label = "Файл"] String filename;
        Form id_form;
    }

    [tableName = pi_images, indexName = id_image, display = Existence, label = "Изображение"]
    class Player_Image {
        Integer id_image;
        [label = "Файл"] String filename;
        [label = "Описание"] String descrition;
        [label = "Название", display = None] String name;
        Form id_form;
    }

    [tableName = p_certificate, label = "Сертификаты"]
    class Certificate {
        [label = "Название"] String name;
        Code data;
        Boolean is_active;
    }
    
    [tableName = p_ages, label = "Возраст", indexName = id_age]
    class Age {
        [label = "Название"] String age;
        [label = "ID"] Integer id_age;
    }

    [tableName = p_forms, label = "Формы", indexName = id_form]
    class Form {
        [label = "Название"] String name;
        [label = "Описание"] String description;
    }
}
