{
    "entry": "output.log",
    "groupFile": "modules/{groupName:lowercase}/{groupName:lowercase}.conf.php",

    "classFile": "modules/{group.groupName:lowercase}/query/{className:lowercase}.php",
    "controller": "modules/{group.groupName:lowercase}/lib/{className:lowercase}.inc.php",

    "searchPage": "modules/{group.groupName:lowercase}/html/{className:lowercase}_search.php",
    "renderListPage": "modules/{group.groupName:lowercase}/html/{className:lowercase}_list.php",
    "renderEditPage": "modules/{group.groupName:lowercase}/html/{className:lowercase}_form.php"
}