var project_name = "sample";
var include_path = "../../js/";
var template_path = "../../templates/";

var ast = require(include_path + "ast.js")
    , meta = require(include_path + "meta-ast.js")
    , tpl = require(include_path + "template.js");

var e = meta.Environment;
ast.initDefaultMethods(e);
tpl.loadTemplate(e, 'sample');
//tpl.loadTemplate(meta.Environment, 'group.stp');
var model = tpl.loadModel(project_name + '.txt');
//console.log(tpl.renderToString(meta.Environment, 'entry', model));
//tpl.renderToFile(meta.Environment, 'entry', model, './output/output.txt');

e.setMap({
    'entry': './output/output.txt',
    'printClass': './output/{className:lowercase}.txt'
});

e.addProduction('entry', model);

e.product.next(function (context, entry, data, file) {
    console.log('output: ', file);
    tpl.renderToFile(context, entry, data, file);
});                                                                         

e.render();

