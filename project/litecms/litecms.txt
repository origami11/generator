
group Publish {
    class Post {
        String title;
        String path;
        Article content;
        DateTime created;
    }

    class Feed {
        String title;
        List<News> news;
    }

    class News {
        String title;
        Article content;
        DateTime created;
    }

    class Template {
        String title;
        Article code;
        Set<Style> styles;
    }

    class Style {
        String title;
        Article code;
    }

    class Bookmark {
        String name;
        Code bookmark;
    }
}